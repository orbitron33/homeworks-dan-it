const prevBtn = document.querySelector("#prev");
const nextBtn = document.querySelector("#next");
const img = document.querySelector(".images-item");
const allImage = document.querySelectorAll(".image-to-show");
console.log(allImage);

let visibleImg = 0;
nextBtn.addEventListener("click", () => {
  visibleImg += 950;
  if (visibleImg > 950 * allImage.length) {
    visibleImg = 0;
  }
  img.style.left = -visibleImg + "px";
});
prevBtn.addEventListener("click", () => {
  visibleImg -= 950;
  if (visibleImg < 0) {
    visibleImg = 950 * allImage.length;
  }
  img.style.left = -visibleImg + "px";
});
