function filterBy(arr, type) {
  return arr.filter((item) => typeof item !== type);
}
let array = ["hello", "world", 23, "23", null, true, { a: 10 }];
let typeElement = "object";
console.log(filterBy(array, typeElement));
