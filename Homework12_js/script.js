const allButtons = document.querySelectorAll(".btn");

document.addEventListener("keyup", function (event) {
  allButtons.forEach((elem) => elem.classList.remove("active"));
  if (document.querySelectorAll(`[data-key="${event.code}"]`)[0]) {
    document
      .querySelectorAll(`[data-key="${event.code}"]`)[0]
      .classList.add("active");
  }
});
