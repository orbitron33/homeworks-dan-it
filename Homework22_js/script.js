const inputButton = document.querySelector(".btn");
const iconPassword = document.querySelectorAll(".icon-password");

inputButton.addEventListener("click", verification);
function verification() {
  if (
    document.getElementById("enter-password").value ===
    document.getElementById("confirm-password").value
  ) {
    document.querySelector(".error").classList.add("invisible");
    alert("You are welcome");
  } else {
    document.querySelector(".error").classList.remove("invisible");
  }
}

iconPassword.forEach((elem) => {
  elem.addEventListener("click", () => {
    elem.classList.contains("fa-eye")
      ? (elem.classList.remove("fa-eye"),
        elem.classList.add("fa-eye-slash"),
        elem.previousElementSibling.setAttribute("type", "text"))
      : (elem.classList.remove("fa-eye-slash"),
        elem.classList.add("fa-eye"),
        elem.previousElementSibling.setAttribute("type", "password"));
  });
});
