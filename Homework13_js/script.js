const imageArr = ["./img/1.jpg", "./img/2.jpg", "./img/3.jpg", "./img/4.png"];
const imageWrapper = document.querySelector(".images-wrapper");
const image = document.getElementById("image");
let counter = 0;
function playImage() {
  counter++;
  if (counter === imageArr.length) {
    counter = 0;
  }
  image.setAttribute("src", imageArr[counter]);
}
let timerId = setInterval(playImage, 3000);
imageWrapper.insertAdjacentHTML(
  "afterend",
  `<button class="push play">Відновити показ</button>`
);
imageWrapper.insertAdjacentHTML(
  "afterend",
  `<button class="push stop">Припинити</button>`
);
document.querySelector(".stop").addEventListener("click", () => {
  clearInterval(timerId);
});
document.querySelector(".play").addEventListener("click", () => {
  clearInterval(timerId);
  timerId = setInterval(playImage, 3000);
});
