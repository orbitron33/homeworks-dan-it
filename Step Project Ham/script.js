const directionTitle = document.querySelectorAll(".direction");
const directionPerent = document.querySelector(".directions-names");
const directionContent = document.querySelectorAll(
  ".direction-content.invisible"
);

directionTitle[0].classList.add("active");
directionContent[0].classList.remove("invisible");

directionPerent.addEventListener("click", function (event) {
  directionTitle.forEach((elemName) => elemName.classList.remove("active"));
  directionContent.forEach((elemText) => {
    if (event.target.dataset.name === elemText.dataset.content) {
      elemText.classList.remove("invisible");
      event.target.classList.add("active");
    } else {
      elemText.classList.add("invisible");
    }
  });
});

const workFilterItem = document.querySelectorAll(".our-work-filter-item");
const workFilterPerent = document.querySelector(".our-work-filter");
const workImages = document.querySelectorAll(".our-work-image-wrapper");
const loadImg = document.querySelector("#load-img");
let workImagesArr = Array.from(workImages);
let count = 12;
loadImg.addEventListener("click", () => {
  if (count >= workImagesArr.length - 12) {
    loadImg.remove();
  }
  count += 12;
  let selectedTab = document.querySelector(
    ".our-work-filter-item.active-filter"
  );
  workImagesArr.forEach((img, idx) => {
    if (
      idx < count &&
      (img.dataset.desing === selectedTab.dataset.title ||
        selectedTab.dataset.title === "All")
    ) {
      img.classList.remove("invisible");
    } else {
      img.classList.add("invisible");
    }
  });
});

workFilterPerent.addEventListener("click", function (event) {
  document
    .querySelector(".our-work-filter-item.active-filter")
    .classList.remove("active-filter");
  event.target.classList.add("active-filter");
  let filterTitle = event.target.dataset["title"];
  workImagesArr.forEach((el, idx) => {
    if (idx < count) {
      el.classList.remove("invisible");
      if (el.dataset.desing !== filterTitle && filterTitle !== "All")
        el.classList.add("invisible");
    }
  });
});

new Swiper(".photo-slider", {
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  thumbs: {
    swiper: {
      el: ".mini-photo-slider",
      slidesPerView: 4,
    },
    slideThumbActiveClass: "swiper-slide-thumb-active",
  },
});
