let m = +prompt("Введіть перше число");
let n = +prompt("Введіть друге число");

while (m > n || m % 1 !== 0 || n % 1 !== 0) {
  m = +prompt("Введіть перше число");
  n = +prompt("Введіть друге число");
}

simplePrime: for (let i = m; i <= n; i++) {
  for (let k = 2; k < i; k++) {
    if (i % k == 0) continue simplePrime;
  }
  console.log(i);
}
