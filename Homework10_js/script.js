const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsPerent = document.querySelector(".tabs");
const personDescription = document.querySelectorAll(".invisible");

tabsTitle[0].classList.add("active");
personDescription[0].classList.remove("invisible");

tabsPerent.addEventListener("click", function (event) {
  tabsTitle.forEach((elemName) => elemName.classList.remove("active"));
  personDescription.forEach((elemText) => {
    if (event.target.dataset.person === elemText.dataset.text) {
      elemText.classList.remove("invisible");
      event.target.classList.add("active");
    } else {
      elemText.classList.add("invisible");
    }
  });
});
