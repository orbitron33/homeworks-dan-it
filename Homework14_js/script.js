const changeTheme = document.querySelector("#btn");
const themeValue = localStorage.getItem("theme");

if (themeValue) {
  document.body.classList.add(themeValue);
}

changeTheme.addEventListener("click", () => {
  if (!localStorage.getItem("theme")) {
    localStorage.setItem("theme", "orange");
    document.body.classList.add("orange");
  } else {
    localStorage.removeItem("theme");
    document.body.classList.remove("orange");
  }
});
