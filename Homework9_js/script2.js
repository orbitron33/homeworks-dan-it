const userArr = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];
function showElementsList(arr, parent = document.body) {
  const newParentList = document.createElement("ol");
  parent.append(newParentList);
  arr.forEach((item, index) => {
    const elem = document.createElement("li");
    elem.innerText = item;
    if (Array.isArray(item)) {
      return showElementsList(
        item,
        newParentList.getElementsByTagName("li")[index - 1]
      );
    } else {
      newParentList.append(elem);
    }
  });
}
showElementsList(userArr);

function removeList() {
  document.body.innerText = "";
}

function showTimer() {
  let count = 3;
  let timerId = setInterval(function () {
    document.getElementById("timer").innerText = count;
    if (count === 0) {
      clearInterval(timerId);
      removeList();
    }
    count--;
  }, 1000);
}
showTimer();
