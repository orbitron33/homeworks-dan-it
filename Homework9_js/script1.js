const userArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function showElementsList(arr, parent = document.body) {
  const newParentList = document.createElement("ol");
  parent.appendChild(newParentList);
  let htmlList = arr.map((item) => `<li>${item}</li>`).join("");
  newParentList.innerHTML = htmlList;
}

showElementsList(userArr);
