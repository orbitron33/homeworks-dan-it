const priceInput = document.querySelector("#price-input");
const inputWrapper = document.querySelector(".input-wrapper");

priceInput.addEventListener("focus", () => {
  priceInput.classList.remove("error");
  priceInput.classList.add("active");
});
priceInput.addEventListener("blur", () => {
  priceInput.classList.remove("active");

  let inputValue = priceInput.value;
  if (inputValue > 0) {
    priceInput.classList.remove("error");
    document.querySelector(".not-correct")?.remove();
    if (document.querySelector(".user-price")) {
      document.querySelector("#value").textContent = inputValue;
    } else {
      inputWrapper.insertAdjacentHTML(
        "beforebegin",
        `<span class="user-price"
      >Поточна ціна: <span id="value">${inputValue}</span> &nbsp; &nbsp; &nbsp;<span class="close-icon">x</span></span
    >`
      );
    }
    priceInput.style.cssText = `
  color: green;
  font-weight: 600;
  font-size: 18px`;
    document.querySelector(".close-icon")?.addEventListener("click", () => {
      document.querySelector(".user-price")?.remove();
      priceInput.value = "";
    });
  } else {
    document.querySelector(".user-price")?.remove();
    if (!document.querySelector(".not-correct")) {
      priceInput.classList.add("error");
      inputWrapper.insertAdjacentHTML(
        "afterend",
        `<span class="not-correct"
      >Please enter correct price</span
    >`
      );
    }
    priceInput.style.cssText = `
      color: red;
      font-weight: 600;
      font-size: 18px`;
  }
});
