function createNewUser() {
  const firstName = prompt("Enter your first name");
  const lastName = prompt("Enter your last name");
  const birthday = prompt("Enter your birthday", "dd.mm.yyyy");
  const today = new Date();
  const birthdayDate = new Date(
    birthday.slice(6),
    birthday.slice(3, 5) - 1,
    birthday.slice(0, 2)
  );
  let newUser = {
    firstName,
    lastName,
    birthday,
    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6)
      );
    },
    getAge() {
      return parseInt(
        (today.getTime() - birthdayDate.getTime()) /
          (24 * 3600 * 365.25 * 1000),
        10
      );
    },
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    setFirstName(firstName) {
      Object.defineProperty(newUser, "firstName", {
        value: firstName,
      });
    },
    setLastName(lastName) {
      Object.defineProperty(newUser, "lastName", {
        value: lastName,
      });
    },
  };

  Object.defineProperty(newUser, "firstName", {
    value: newUser.firstName,
    writable: false,
  });
  Object.defineProperty(newUser, "lastName", {
    value: newUser.lastName,
    writable: false,
  });
  return newUser;
}
let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getPassword());
console.log(newUser.getAge());
