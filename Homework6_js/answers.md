1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.

   # Відповідь

   Це метод що дозволяє використовувати для відображення користувачу різні символи і знаки а інтерпритатор не сприймав його як спаціальний символ коду і не намагався провести з ним якусь дію.

2. Які засоби оголошення функцій ви знаєте?

   # Відповідь

   Знаю 3 засоби оголошення функції:

   - Function Declaration ( function ім'я функції (параметри) { інструкції } );
   - Function Expression ( let/const ім'я = function (параметри) { інструкції } );
   - Named Function Expression ( let/const ім'я = function ім'я функції (параметри) { інструкції } );

3. Що таке hoisting, як він працює для змінних та функцій?

   # Відповідь

   hoisting це механізм за яким JavaScript переставляє оголошення змінних і функцій вверх своєї області видимості, не залежно кід того коли вони були оголошені. Змінні let і const піднімуться вгору але не працюватимуть якщо їх викликати перед створенням. Інтерпритатор видасть помилку про те що ми хочемо викликати функцію перед оголошенням. Функції оголошені методом Function Declaration працюватимуть якщо їх викликати перед місцем оголошення.
