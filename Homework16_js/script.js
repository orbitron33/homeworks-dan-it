let firstNumber = prompt("Enter first numder");
while (!firstNumber || isNaN(firstNumber)) {
  firstNumber = prompt("Enter first numder, again", firstNumber);
}
let secondNumber = prompt("Enter second numder");
while (!secondNumber || isNaN(secondNumber)) {
  secondNumber = prompt("Enter second numder, again", secondNumber);
}
let xNumber = prompt("Enter number to find");
while (!xNumber || isNaN(xNumber)) {
  xNumber = prompt("Enter number to find, again");
}

function count(F0, F1, n) {
  let a = F0;
  let b = F1;
  let c;
  for (let i = 0; i < n; i++) {
    c = a + b;
    a = b;
    b = c;
  }
  return c;
}
console.log(count(+firstNumber, +secondNumber, +xNumber));
