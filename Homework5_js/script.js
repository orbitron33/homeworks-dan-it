function createNewUser() {
  let firstName = prompt("Enter your first name");
  let lastName = prompt("Enter your last name");
  let newUser = {
    firstName,
    lastName,
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    setFirstName(firstName) {
      Object.defineProperty(newUser, "firstName", {
        value: firstName,
      });
    },
    setLastName(lastName) {
      Object.defineProperty(newUser, "lastName", {
        value: lastName,
      });
    },
  };

  Object.defineProperty(newUser, "firstName", {
    value: newUser.firstName,
    writable: false,
  });
  Object.defineProperty(newUser, "lastName", {
    value: newUser.lastName,
    writable: false,
  });
  return newUser;
}
let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
