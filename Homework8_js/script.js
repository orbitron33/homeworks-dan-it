const elemParagraph = document.getElementsByTagName("p");
const arrayParagraph = Array.from(elemParagraph).forEach((element) => {
  element.style.backgroundColor = "#ff0000";
});
const elemList = document.getElementById("optionsList");
console.log(elemList);
console.log(elemList.parentElement);
const nodesOfOptionalList = elemList.childNodes;
nodesOfOptionalList.forEach((node) =>
  console.log(`nodeName: ${node.nodeName},
  nodeType: ${node.nodeType}`)
);
const elemTestParagraph = document.getElementById("testParagraph");
elemTestParagraph.innerText = "This is a paragraph";

const elemMainHeader = document.querySelector(".main-header");
console.log(elemMainHeader.children);
const childsOfMainHeader = Array.from(elemMainHeader.children);
childsOfMainHeader.forEach((child) => child.classList.add("nav-item"));
console.log(childsOfMainHeader);

let sectTitle = document.getElementsByClassName("section-title");
const arrSectTitle = Array.from(sectTitle);
arrSectTitle.forEach((elem) => elem.classList.remove("section-title"));
console.log(arrSectTitle);
