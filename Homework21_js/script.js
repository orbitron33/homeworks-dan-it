const btn = document.querySelector(".draw");
btn.insertAdjacentHTML("afterEnd", `<div class="wrapper"></div>`);
const circlesWrapper = document.querySelector(".wrapper");

function generateColor() {
  return "#" + Math.floor(Math.random() * 16777215).toString(16);
}

btn.addEventListener("click", () => {
  const valueDiameter = prompt("Введіть діаметр кола");
  if (!Number.isNaN(valueDiameter) && !!valueDiameter) {
    for (let i = 1; i <= 100; i++) {
      circlesWrapper.insertAdjacentHTML(
        "afterBegin",
        `<div class="circle"></div>`
      );
      document.querySelector(".circle").style.cssText = `
        background-color: ${generateColor()};
        width: ${valueDiameter}px;
        height: ${valueDiameter}px;
        border: 1px solid;
        border-radius: 50%;
      `;
    }
  }
});
circlesWrapper.addEventListener("click", (event) => {
  if (event.target != event.currentTarget) event.target.remove();
});
